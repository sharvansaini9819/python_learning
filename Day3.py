
# name="ritu yadav"

# new_name=name.replace("yadav","b")
# print(new_name)
# print("ri" in name)

# print(5*3)
# print(5**3)

#and,or,not

#conditions loops
a = 10
b = 20
c = "%"
# if a < b:
#     print("yes")
# else:
#     print("no")

# marks = 51
# if marks<=100 and marks>90:
#     print("Grade A")
# elif marks<=90 and marks>80:
#     print("Grade B")
# elif marks<=80 and marks>70:
#     print("Grade C")
# elif marks<=70 and marks>60:
#     print("Grade C+")
# elif marks<=60 and marks>50:
#     print("Grade D")
# else:
#     print("Grade F")


# if c=="+":
#     print(a+b)
# elif c=="-":
#     print(a-b)
# elif c=="*":
#     print(a*b)
# elif c=="/":
#     print(a/b)
# elif c=="%":
#     print(a%b)
# else:
#     print("worng operator")

# age =18
# if not age:
#     print("no")
# else:
#     print("yes")

# iterative loops

# while and for -> foreach
# x=10
# print(x)
# x=x+1
# print(x)

# i=1   # initialisation
# while i <= 10:   # condition
#     print(i)
#     i=i+1      # inc
#
# i = 10   # initialisation
# while i >= 1:   # condition
#     print(i)
#     i=i-1      # dec

# even no in range
# i =1
# while i<=50:
#     if i%2==0:
#         print(i)
#     i=i+1

# odd no in range
# i =1
# while i<=50:
#     if i%2!=0 :
#         print(i)
#     i=i+1

# i = 1
# while i<=50:
#     if i%5==0 or i%10==0:
#         print(i)
#     i+=1

#for loop

# for x in range(11):
#     print(x*2)

# for x in range(1,11):
#      print(x)

# for x in range(1,11,2):
#      print(x)


# name="ritu yadav"
# print(len(name))
# for each
# for letter in name:
#     print(letter)


# array/list->[]

first_name="ritu"
last_name="yadav"
roll_n0=10

details = [10,"Ritu","yadav",7.8,"Gurgoan","GU","sd","yadav","yadav","yadav"]


#print(details)
# print(details[0])
# print(details[1])
# print(details[2])
# print(details[3])

# for val in details:
#     print(val)
# print("---------------------------")
# details.append(45)
# details.remove("GU")
#
# for val in details:
#     print(val)
#
# # count the occurrence of any element
# print(details.count("yadav"))
# print("---------------------------")
# details.reverse()
# for val in details:
#     print(val)


# #total element present in list
# print(len(details))
#
# #backside print
# print(details[-5])
# #within range print
# print(details[3:6])

if "MBA" not in details:
    details.append("MBA")

# list=[1,2,3,4,5,6,7,8,9,10]
#
# ans=[]
#
# for val in list:
#     if val%2==0:
#         ans.append(val)
#
# print(ans)


















